<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo "Antrian" ?></h1>
                   
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3>Daftar Antrian</h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>

                                                <th>Nama Klinik</th>
                                                <th>Nama Dokter</th>
                                                <th>No Antrian</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>
                                         <tbody>
<?php foreach ($antrians as $antrian):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($antrian->nama_klinik, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($antrian->nama_dokter, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($antrian->no_antrian, ENT_QUOTES, 'UTF-8'); ?></td>
                                              
                                                <td>
                                                    <a href="<?php echo base_url('admin/antrians/tambahdata/'.$antrian->id_antrian); ?>"><button style="padding: 5" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></a>
                                                    

                                                    <span> </span>

                                                    <a href="<?php echo base_url('admin/antrians/kurangdata/'.$antrian->id_antrian); ?>">
                                                        <button style="padding: 5" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button></a>


                                                    <span> </span>


                                                    <a href="<?php echo base_url('admin/antrians/resetdata/'.$antrian->id_antrian); ?>">
                                                        <button style="padding: 5" type="button" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-repeat"></span></button></a>


                                                    

                                                </td>
                                                

<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
