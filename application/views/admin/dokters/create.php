<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('users_create_dokters'); ?></h3>
                                </div>
                                <div class="box-body">

                                    <?php echo form_open('admin/dokters/tambahdokter', ['class' => 'form-horizontal', 'method' => 'post']); ?>
                                        

                                        <div class="form-group <?php echo (form_error('kd_dokter') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="kd_dokter" class="control-label col-sm-2">Kode Dokter</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="kd_dokter" value="<?php echo set_value('kd_dokter'); ?>">
                                                <?php echo (form_error('kd_dokter') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('kd_dokter'); ?>
                                            </div>
                                        </div>



                                        

                                        <div class="form-group <?php echo (form_error('nama_dokter') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="nama_dokter" class="control-label col-sm-2">Nama Dokter </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="nama_dokter" value="<?php echo set_value('nama_dokter'); ?>">
                                                <?php echo (form_error('nama_dokter') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('nama_dokter'); ?>
                                            </div>
                                        </div>

                                        <div class="form-group <?php echo (form_error('umur') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="umur" class="control-label col-sm-2">Umur</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="umur" value="<?php echo set_value('umur'); ?>">
                                                <?php echo (form_error('umur') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('umur'); ?>
                                            </div>
                                        </div>



                                        <div class="form-group <?php echo (form_error('spesialis') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="spesialis" class="control-label col-sm-2">Spesialis </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="spesialis" value="<?php echo set_value('spesialis'); ?>">
                                                <?php echo (form_error('spesialis') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('spesialis'); ?>
                                            </div>
                                        </div>

                                        <div class="form-group <?php echo (form_error('alamat') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="alamat" class="control-label col-sm-2">Alamat </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="alamat" value="<?php echo set_value('alamat'); ?>">
                                                <?php echo (form_error('alamat') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('alamat'); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2"></div>
                                            <div class="btn-form col-sm-10">
                                                <a href="<?php echo base_url('admin/dokters'); ?>">
                                                <button type="button" class='btn btn-default'>Batal</button></a>
                                                <span> </span>
                                                <button type="submit" class='btn btn-primary'>Simpan</button>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
