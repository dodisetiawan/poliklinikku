<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Dokter</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Dokter</h3>
                                </div>
                                <div class="box-body">

                                <?php echo form_open('admin/dokters/updatedokter/'.$db->id_dokter, ['class' => 'form-horizontal', 'method' => 'post']); ?>
                            


                                        <div class="form-group <?php echo (form_error('id_dokter') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="id_dokter" class="control-label col-sm-2">Id Dokter</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="id_dokter" value="<?php echo set_value('id_dokter', $db->id_dokter); ?>" readonly>
                                                <?php echo (form_error('id_dokter') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?> 
                                                <?php echo form_error('id_dokter'); ?>
                                            </div>
                                        </div>




                                        
                                        <div class="form-group <?php echo (form_error('kd_dokter') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="kd_dokter" class="control-label col-sm-2">Kode Dokter</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="kd_dokter" value="<?php echo set_value('kd_dokter', $db->kd_dokter); ?>" readonly>
                                                <?php echo (form_error('kd_dokter') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?> 
                                                <?php echo form_error('kd_dokter'); ?>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <label for="nama_dokter" class="control-label col-sm-2">Nama Dokter </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="nama_dokter" value="<?php echo set_value('nama_dokter', $db->nama_dokter); ?>">
                                                <?php echo form_error('nama_dokter'); ?>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="umur" class="control-label col-sm-2">Umur </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="umur" value="<?php echo set_value('umur', $db->umur); ?>">
                                                <?php echo form_error('umur'); ?>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="spesialis" class="control-label col-sm-2">Spesialis </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="spesialis" value="<?php echo set_value('spesialis', $db->spesialis); ?>">
                                                <?php echo form_error('spesialis'); ?>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="alamat" class="control-label col-sm-2">Alamat</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="alamat" value="<?php echo set_value('alamat', $db->alamat); ?>">
                                                <?php echo form_error('alamat'); ?>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="btn-form col-sm-10">
                                                <a href="<?php echo base_url('admin/dokters'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
                                                <span> </span>
                                                <button type="submit" class='btn btn-primary'>Simpan</button>
                                            </div>
                                        </div>



                        <?php echo form_close(); ?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
