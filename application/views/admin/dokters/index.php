<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo "Dokters" ?></h1>
                   
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('admin/dokters/create', '<i class="fa fa-plus"></i> '. lang('dokters_create'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>

                                                <th>Id Dokter</th>
                                                <th><?php echo lang('dokters_id');?></th>
                                                <th><?php echo lang('dokters_name');?></th>
                                                <th><?php echo lang('dokters_umur');?></th>
                                                <th><?php echo lang('dokters_spesialis');?></th>
                                                <th><?php echo lang('dokters_alamat');?></th>
                                                <th><?php echo lang('users_action');?></th>
                                            </tr>

                                        </thead>
                                         <tbody>
<?php foreach ($dokters as $dokter):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($dokter->id_dokter, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($dokter->kd_dokter, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($dokter->nama_dokter, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($dokter->umur, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($dokter->spesialis, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($dokter->alamat, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('admin/dokters/formedit/'.$dokter->id_dokter); ?>"><button style="padding: 5" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
                                                    

                                                    <span> </span>

                                                    <a href="<?php echo base_url('admin/dokters/hapusdata/'.$dokter->id_dokter); ?>" onclick="return confirm('Anda yakin hapus ?')"><button style="padding: 5" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button></a>
                                                </td>
                                                

<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
