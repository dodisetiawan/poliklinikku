<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('users_create_klinik'); ?></h3>
                                </div>
                                <div class="box-body">

                                    <?php echo form_open('admin/kliniks/tambahklinik', ['class' => 'form-horizontal', 'method' => 'post']); ?>
                                        

                                        <div class="form-group <?php echo (form_error('nama_klinik') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="nama_klinik" class="control-label col-sm-2">Nama Klinik</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="nama_klinik" value="<?php echo set_value('nama_klinik'); ?>">
                                                <?php echo (form_error('nama_klinik') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('nama_klinik'); ?>
                                            </div>
                                        </div>



                                        <div class="form-group <?php echo (form_error('alamat') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="alamat" class="control-label col-sm-2">Alamat</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="alamat" value="<?php echo set_value('alamat'); ?>">
                                                <?php echo (form_error('alamat') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
                                                <?php echo form_error('alamat'); ?>
                                            </div>
                                        </div>

                                      

                                        <div class="form-group">
                                            <div class="col-md-2"></div>
                                            <div class="btn-form col-sm-10">
                                                <a href="<?php echo base_url('admin/kliniks'); ?>">
                                                <button type="button" class='btn btn-default'>Batal</button></a>
                                                <span> </span>
                                                <button type="submit" class='btn btn-primary'>Simpan</button>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
