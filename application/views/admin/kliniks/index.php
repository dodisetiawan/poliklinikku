<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo "Kliniks" ?></h1>
                   
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('admin/kliniks/create', '<i class="fa fa-plus"></i> '. lang('kliniks_create'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('kliniks_id');?></th>
                                                <th><?php echo lang('nama_kliniks');?></th>
                                                <th><?php echo lang('alamat');?></th>
                                                <th><?php echo lang('users_action');?></th>
                                            </tr>

                                        </thead>
                                         <tbody>
<?php foreach ($kliniks as $klinik):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($klinik->idklinik, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($klinik->nama_klinik, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($klinik->alamat, ENT_QUOTES, 'UTF-8'); ?></td>
                                              
                                                <td>
                                                    <a href="<?php echo base_url('admin/kliniks/formedit/'.$klinik->idklinik); ?>"><button style="padding: 5" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
                                                    

                                                    <span> </span>

                                                    <a href="<?php echo base_url('admin/kliniks/hapusdata/'.$klinik->idklinik); ?>" onclick="return confirm('Anda yakin hapus ?')">
                                                        <button style="padding: 5" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button></a>


                                                    <span> </span>


                                                    <a href="<?php echo base_url('admin/kliniks/formklinikdokter/'.$klinik->idklinik); ?>"><button style="padding: 6" type="button" class="btn btn-default btn-xs"><span class="fa fa-address-card"></span></button></a>

                                                </td>
                                                

<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
