<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Detail Klinik</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Klinik</h3>
                                </div>
                                <div class="box-body">

                                <?php echo form_open('admin/kliniks/updateklinik/'.$db->idklinik, ['class' => 'form-horizontal', 'method' => 'post']); ?>
                            


                                        <div class="form-group <?php echo (form_error('idklinik') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="idklinik" class="control-label col-sm-2">Id Klinik</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="idklinik" value="<?php echo set_value('idklinik', $db->idklinik); ?>" readonly>
                                                <?php echo (form_error('idklinik') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?> 
                                                <?php echo form_error('idklinik'); ?>
                                            </div>
                                        </div>


                                         <div class="form-group">
                                            <label for="nama_klinik" class="control-label col-sm-2">Nama Klinik</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="nama_klinik" value="<?php echo set_value('nama_klinik', $db->nama_klinik); ?>" readonly>
                                                <?php echo form_error('nama_klinik'); ?>
                                            </div>
                                        </div>

                                        
                                       

                                        <div class="form-group">
                                            <label for="alamat" class="control-label col-sm-2">Alamat</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="alamat" value="<?php echo set_value('alamat', $db->alamat); ?>" readonly>
                                                <?php echo form_error('alamat'); ?>
                                            </div>
                                        </div>

                        <?php echo form_close(); ?>



                                </div>


                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('admin/kliniks/formtambahdokter/'.$db->idklinik, '<i class="fa fa-plus"></i> '. lang('add_dokter'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                               
                                                <th>Id Dokter</th>
                                                <th>Nama Dokter</th><!-- 
                                                <th>Kode Dokter</th> -->
                                                <th>Spesialis</th>
                                                <th>Jlm Antrian</th>

                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                         <tbody>
<?php foreach ($details as $detail):?>
                                            <tr>
                                                
                                                <td><?php echo htmlspecialchars($detail->id_dokter, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($detail->nama_dokter, ENT_QUOTES, 'UTF-8'); ?></td><!-- 
                                                <td><?php echo htmlspecialchars($detail->kd_dokter, ENT_QUOTES, 'UTF-8'); ?></td> -->
                                                <td><?php echo htmlspecialchars($detail->spesialis, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($detail->jml_antrian, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td style="text-align: left;">
                                           

                                                    <a href="<?php echo base_url('admin/kliniks/hapusdatadetail/'.$detail->id_detail_kliniks); ?>" onclick="return confirm('Anda yakin hapus ?')"><button style="padding: 5" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button></a>
                                                </td>
                                                

<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>





                            </div>
                         </div>
                    </div>
                </section>
            </div>





