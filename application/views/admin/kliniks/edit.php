<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Klinik</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Klinik</h3>
                                </div>
                                <div class="box-body">

                                <?php echo form_open('admin/kliniks/updateklinik/'.$db->idklinik, ['class' => 'form-horizontal', 'method' => 'post']); ?>
                            


                                        <div class="form-group <?php echo (form_error('idklinik') != '') ? 'has-error has-feedback' : '' ?>">
                                            <label for="idklinik" class="control-label col-sm-2">Id Klinik</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="idklinik" value="<?php echo set_value('idklinik', $db->idklinik); ?>" readonly>
                                                <?php echo (form_error('idklinik') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?> 
                                                <?php echo form_error('idklinik'); ?>
                                            </div>
                                        </div>


                                         <div class="form-group">
                                            <label for="nama_klinik" class="control-label col-sm-2">Nama Klinik</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="nama_klinik" value="<?php echo set_value('nama_klinik', $db->nama_klinik); ?>">
                                                <?php echo form_error('nama_klinik'); ?>
                                            </div>
                                        </div>

                                        
                                       

                                        <div class="form-group">
                                            <label for="alamat" class="control-label col-sm-2">Alamat</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="alamat" value="<?php echo set_value('alamat', $db->alamat); ?>">
                                                <?php echo form_error('alamat'); ?>
                                            </div>
                                        </div>


                                




                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="btn-form col-sm-10">
                                                <a href="<?php echo base_url('admin/kliniks'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
                                                <span> </span>
                                                <button type="submit" class='btn btn-primary'>Simpan</button>
                                            </div>
                                        </div>



                        <?php echo form_close(); ?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
