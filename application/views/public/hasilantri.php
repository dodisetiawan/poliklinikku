<!DOCTYPE html>
<html>
<head>
	<title></title>

	 <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">


</head>
<body>
	<section class="container-fluid">
	<div class="row">
		<div class="col-md-12">


			<div class="panel panel-primary">
				<div class="panel-heading">Hasil</div>
				<div class="panel-body">

					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Nama Klinik</th>
										<th>Nama Dokter</th>
										<th>Spesialis</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										$no = 1;
										foreach($datahasil as $klinik) : ?>
											<tr>
												<td><?php echo $klinik->nama_klinik?></td>
												<td><?php echo $klinik->nama_dokter; ?></td>
												<td><?php echo $klinik->spesialis; ?></td>
											</tr>
											<tr>
												<td colspan="3" align="center" >
													<h3>No Antrian</h3>
												</td>
											</tr>
											<tr>
												<td colspan="3" align="center" >
													<h1><?php echo $klinik->no_antrian; ?></h1>
												</td>
											</tr>

									<?php
										$no++;
										endforeach;
									?>
								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>


</body>
</html>