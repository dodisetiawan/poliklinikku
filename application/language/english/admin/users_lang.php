<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dokters_id']           = 'Kode Dokter';
$lang['users_action']              = 'Action';
$lang['users_active']              = 'Active';
$lang['users_company']             = 'Company';
$lang['users_create_user']         = 'Create User';
$lang['users_created_on']          = 'Created on';
$lang['users_deactivate_question'] = 'Are you sure you want to deactivate the user %s';
$lang['users_edit_user']           = 'Edit user';
$lang['users_email']               = 'Email';
$lang['users_firstname']           = 'First name';
$lang['users_groups']              = 'Groups';
$lang['users_inactive']            = 'Inactive';
$lang['users_ip_address']          = 'IP address';
$lang['users_last_login']          = 'Last login';
$lang['users_lastname']            = 'Last name';
$lang['users_member_of_groups']    = 'Member of groups';
$lang['users_password']            = 'Password';
$lang['users_password_confirm']    = 'Password confirm';
$lang['users_phone']               = 'Phone';
$lang['users_status']              = 'Status';
$lang['users_username']            = 'User name / Pseudo';



$lang['users_create_dokters']         = 'Create Dokter';

$lang['dokters_id']           = 'Kode Dokter';
$lang['dokters_umur']           = 'Umur';
$lang['dokters_name']           = 'Nama Dokter';
$lang['dokters_spesialis']           = 'Spesialis';
$lang['dokters_alamat']           = 'Alamat';
$lang['dokters_id_klnik']           = 'Id Klinik';
$lang['dokters_create']           = 'Create Dokter';
$lang['dokters_keahlian']             = 'Keahlian';

$lang['kliniks_create']           = 'Add Klinik';
$lang['kliniks_id']           = 'Id Klinik';
$lang['nama_kliniks']           = 'Nama Klinik';
$lang['alamat']           = 'Alamat Klinik';

$lang['users_create_klinik']         = 'Create Klinik';



$lang['id_detail_kliniks']           = 'Id Details';
$lang['idklinik']           = 'Id Klinik';
$lang['id_dokter']           = 'Id Dokter';

$lang['add_dokter']           = 'Add Dokter Klinik';






