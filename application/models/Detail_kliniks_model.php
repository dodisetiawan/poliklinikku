<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Detail_kliniks_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('detail_kliniks');
		return $query->result();
	}
	
	public function get_data_kliniks($idklinik)
	{
		$this->db->select('*');
		$this->db->from('detail_kliniks');

		$this->db->join('dokters', 'dokters.id_dokter = detail_kliniks.id_dokter');
		



		$this->db->where('idklinik',$idklinik);
		$query=$this->db->get();
		return $query->result();
	}

	
	public function tambah_klinik_dokter()
	{
		$data = [
					'idklinik' => $this->input->post('idklinik'),
					'id_dokter' => $this->input->post('id_dokter'),
				];

		$this->db->insert('detail_kliniks', $data);
	}

	public function hapus_dokter_klinik($id_detail_kliniks)
	{
		$this->db->delete('detail_kliniks', ['id_detail_kliniks' => $id_detail_kliniks]);
	}



	



    
}
