<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dokters_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('dokters');
		return $query->result();
	}

	public function tambah_dokter()
	{
		$data = [
					'kd_dokter' => $this->input->post('kd_dokter'),
					'nama_dokter' => $this->input->post('nama_dokter'),
					'umur' => $this->input->post('umur'),
					'spesialis' => $this->input->post('spesialis'),
					'alamat' => $this->input->post('alamat'),
				];

		$this->db->insert('dokters', $data);
	}


	public function edit_dokter($id_dokter)
	{
		$query = $this->db->get_where('dokters', ['id_dokter' => $id_dokter]);
		return $query->row();
	}

	public function update_dokter()
	{
		$kondisi = ['id_dokter' => $this->input->post('id_dokter')];
		
		$data = [
					'kd_dokter' => $this->input->post('kd_dokter'),
					'nama_dokter' => $this->input->post('nama_dokter'),
					'umur' => $this->input->post('umur'),
					'spesialis' => $this->input->post('spesialis'),
					'alamat' => $this->input->post('alamat')
				];

		$this->db->update('dokters', $data, $kondisi);
	}

	public function hapus_dokter($id_dokter)
	{
		$this->db->delete('dokters', ['id_dokter' => $id_dokter]);
	}

	function get_option() {
	 $this->db->select('*');
	 $this->db->from('dokters');
	 $query = $this->db->get();
	 return $query->result();
	}

	



    
}
