<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kliniks_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('kliniks');
		return $query->result();
	}

	public function tambah_klinik()
	{
		$data = [
					'nama_klinik' => $this->input->post('nama_klinik'),
					'alamat' => $this->input->post('alamat'),
				];

		$this->db->insert('kliniks', $data);
	}


	public function edit_klinik($idklinik)
	{
		$query = $this->db->get_where('kliniks', ['idklinik' => $idklinik]);
		return $query->row();
	}

	public function update_klinik()
	{
		$kondisi = ['idklinik' => $this->input->post('idklinik')];
		
		$data = [
					'nama_klinik' => $this->input->post('nama_klinik'),
					'alamat' => $this->input->post('alamat')
				];

		$this->db->update('kliniks', $data, $kondisi);
	}

	public function hapus_klinik($idklinik)
	{
		$this->db->delete('kliniks', ['idklinik' => $idklinik]);
	}

	



    
}
