<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Antrians_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('detail_kliniks');
		return $query->result();
	}
	
	public function get_data_antrians()
	{
		$this->db->select('*');
		$this->db->from('antrian');

		$this->db->join('detail_kliniks', 'detail_kliniks.id_detail_kliniks = antrian.id_detail_kliniks', 'left' );
		$this->db->join ( 'dokters', 'dokters.id_dokter = detail_kliniks.id_dokter' , 'left' );
		$this->db->join ( 'kliniks', 'kliniks.idklinik = detail_kliniks.idklinik' , 'left' );
		
		$query=$this->db->get();
		return $query->result();
	}

		public function get_data_hasil($id_detail_kliniks)
	{
		$this->db->select('*');
		$this->db->from('antrian');

		$this->db->join('detail_kliniks', 'detail_kliniks.id_detail_kliniks = antrian.id_detail_kliniks', 'left' );
		$this->db->join ( 'dokters', 'dokters.id_dokter = detail_kliniks.id_dokter' , 'left' );
		$this->db->join ( 'kliniks', 'kliniks.idklinik = detail_kliniks.idklinik' , 'left' );
		



		$this->db->where('antrian.id_detail_kliniks',$id_detail_kliniks);
		$query=$this->db->get();
		return $query->result();
	}


	
	public function tambah_klinik_dokter()
	{
		$data = [
					'idklinik' => $this->input->post('idklinik'),
					'id_dokter' => $this->input->post('id_dokter'),
				];

		$this->db->insert('detail_kliniks', $data);
	}

	public function hapus_dokter_klinik($id_detail_kliniks)
	{
		$this->db->delete('detail_kliniks', ['id_detail_kliniks' => $id_detail_kliniks]);
	}



	public function tambah_data_antri($id_detail_kliniks)
	{
		$this->db->set('no_antrian', 'no_antrian+1', FALSE);
		$this->db->where('id_detail_kliniks', $id_detail_kliniks);
		$this->db->update('antrian'); 
	}




	public function tambahdata($id_antrian)
	{
		$this->db->set('no_antrian', 'no_antrian+1', FALSE);
		$this->db->where('id_antrian', $id_antrian);
		$this->db->update('antrian'); 
	}

		public function kurangdata($id_antrian)
	{
		$this->db->set('no_antrian', 'no_antrian-1', FALSE);
		$this->db->where('id_antrian', $id_antrian);
		$this->db->update('antrian'); 
	}


		public function resetdata($id_antrian)
	{
		$this->db->set('no_antrian', '1', FALSE);
		$this->db->where('id_antrian', $id_antrian);
		$this->db->update('antrian');  
	}



	



    
}
