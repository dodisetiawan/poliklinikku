<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kliniks extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('kliniks_create'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('kliniks_create'), 'admin/users');

		$this->load->model('kliniks_model');
		$this->load->model('detail_kliniks_model');
		$this->load->helper(['url_helper', 'form']);




    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all users */

            $data['kliniks'] = $this->kliniks_model->get_all_data();

            $this->load->view('admin/kliniks/index', $data);




            // $this->data['users'] = $this->ion_auth->users()->result();
            // foreach ($this->data['users'] as $k => $user)
            // {
            //     $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            // }

            /* Load Template */
            $this->template->admin_render('admin/kliniks/index', $this->data);
        }
	}


	public function create()
	{
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_create'), 'admin/users/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        $tables = $this->config->item('tables', 'ion_auth');



        

            /* Load Template */
        $this->template->admin_render('admin/kliniks/create', $this->data);
      }
     

    public function tambahklinik(){

    	$this->form_validation->set_message('is_unique', '{field} sudah terpakai');

		// $this->form_validation->set_rules('idklinik', 'Kode Dokter', ['required', 'is_unique[dokters.kd_dokter']);

		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->create();
		}
		else
		{
			$this->kliniks_model->tambah_klinik();
			redirect('/admin/kliniks', 'refresh');
		}

    }


    public function validasi()
	{
		$this->form_validation->set_message('required', '{field} tidak boleh kosong');

		$config = [
				[
					'field' => 'nama_klinik',
					'label' => 'Nama Klinik',
					'rules' => 'required'
				],
				[
					'field' => 'alamat',
					'label' => 'Alamat',
					'rules' => 'required'
				]];

		$this->form_validation->set_rules($config);
	}






	



	public function hapusdata($idklinik)
	{
		$this->kliniks_model->hapus_klinik($idklinik);
		redirect('/admin/kliniks', 'refresh');
	}

	public function hapusdatadetail($id_detail_kliniks)
	{
		$this->detail_kliniks_model->hapus_dokter_klinik($id_detail_kliniks);
		redirect('/admin/kliniks/', 'refresh');
	}


	
	public function formedit($idklinik)
	{
		$data['title'] = 'Edit Data | Test tampil Database';

		$data['db'] = $this->kliniks_model->edit_klinik($idklinik);
		$this->load->view('admin/kliniks/edit', $data);


        $this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'admin/kliniks/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		$this->template->admin_render('admin/kliniks/edit', $this->data);

	}




	public function formklinikdokter($idklinik)
	{
		$data['title'] = 'Edit Data | Test tampil Database';

		$data['db'] = $this->kliniks_model->edit_klinik($idklinik);

        $data['details'] = $this->detail_kliniks_model->get_data_kliniks($idklinik);

		$this->load->view('admin/kliniks/adddokter', $data);


        $this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'admin/kliniks/adddokter');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		$this->template->admin_render('admin/kliniks/adddokter', $this->data);

	}



	  public function tambah_klinik_dokter(){

    	//$this->form_validation->set_message('is_unique', '{field} sudah terpakai');

		//$this->form_validation->set_rules('id_dokter', 'Kode Dokter', ['required', 'is_unique[detail_kliniks.kd_dokter']);

		//$this->validasi();

		// if($this->form_validation->run() === FALSE)
		// {
		// 	$this->formklinikdokter();
		// }
		// else
		// {
			$this->detail_kliniks_model->tambah_klinik_dokter();
			redirect('/admin/kliniks/', 'refresh');
		// }

    }





	public function formtambahdokter($idklinik)
	{







		$data['title'] = 'Edit Data | Test tampil Database';

		$data['db'] = $this->kliniks_model->edit_klinik($idklinik);
		$this->load->view('admin/kliniks/pilihdokter', $data);


        $this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'admin/kliniks/pilihdokter');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		$this->template->admin_render('admin/kliniks/pilihdokter', $this->data);

	}



	public function updateklinik($idklinik)
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formedit($idklinik);
		}
		else
		{
			$this->kliniks_model->update_klinik();
			redirect('/admin/kliniks', 'refresh');
		}
	}










	function activate($id, $code = FALSE)
	{
        $id = (int) $id;

		if ($code !== FALSE)
		{
            $activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
            $this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect('admin/users', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect('auth/forgot_password', 'refresh');
		}
	}


	public function deactivate($id = NULL)
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
            return show_error('You must be an administrator to view this page.');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_deactivate'), 'admin/users/deactivate');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
		$this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

		$id = (int) $id;

		if ($this->form_validation->run() === FALSE)
		{
			$user = $this->ion_auth->user($id)->row();

            $this->data['csrf']       = $this->_get_csrf_nonce();
            $this->data['id']         = (int) $user->id;
            $this->data['firstname']  = ! empty($user->first_name) ? htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8') : NULL;
            $this->data['lastname']   = ! empty($user->last_name) ? ' '.htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->template->admin_render('admin/users/deactivate', $this->data);
		}
		else
		{
            if ($this->input->post('confirm') == 'yes')
			{
                if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id'))
				{
                    show_error($this->lang->line('error_csrf'));
				}

                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			redirect('admin/users', 'refresh');
		}
	}


	public function profile($id)
	{
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_profile'), 'admin/groups/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $id = (int) $id;

        $this->data['user_info'] = $this->ion_auth->user($id)->result();
        foreach ($this->data['user_info'] as $k => $user)
        {
            $this->data['user_info'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }

        /* Load Template */
		$this->template->admin_render('admin/users/profile', $this->data);
	}


	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}


	public function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}



/* End of file Dokters.php */
/* Location: ./application/controllers/admin/Dokters.php */



