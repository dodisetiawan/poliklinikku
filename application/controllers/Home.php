<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller {

    public function __construct()
    {

        parent::__construct();



		$this->load->model('kliniks_model');

		$this->load->model('antrians_model');

		$this->load->model('detail_kliniks_model');
    }


	public function index()
	{

		$this->load->view('public/home', $this->data);
	}

	public function kliniks()
	{


        $data['kliniks'] = $this->kliniks_model->get_all_data();

		$this->load->view('public/kliniks', $data);
	}



	public function dokterklinik($idklinik)
	{


        $data['details'] = $this->detail_kliniks_model->get_data_kliniks($idklinik);

		$this->load->view('public/klinikdokter', $data);




	}

	public function antri($id_detail_kliniks)
	{


		$this->antrians_model->tambah_data_antri($id_detail_kliniks);
		redirect('home/hasilantri/'.$id_detail_kliniks);




	}

		public function hasilantri($id_detail_kliniks)
	{

		$data['datahasil'] = $this->antrians_model->get_data_hasil($id_detail_kliniks);
		$this->load->view('public/hasilantri', $data);


		// $this->antrians_model->tambah_data_antri($id_detail_kliniks);
		// redirect('home/hasilantri/'.$id_detail_kliniks);




	}
}

